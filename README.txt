# Pasat_Vlad-Marian_Aplicatie_Practica_AIA_Licenta

## Adresa repository
https://gitlab.upt.ro/vlad.pasat/pasat_vlad-marian_aplicatie_practica_aia_licenta/-/tree/main

## Necesități

Python 3.7
pip 23.1.2 
PyCharm 2022.3.2 
OpenCV 
MediaPipe 
Arduino IDE

În terminal se rulează următoarele comenzi pentru instalarea necesităților:

```
pip install opencv-python
pip install mediapipe
pip install tensorflow

```

## Rularea aplicației 

După ce toate dependențele au fost instalate, fișierul handTrackingModule.py va fi rulat fie din PyCharm, fie din consolă folosind py handTrackingModule.py. Este necesar ca versiunea Python să fie 3.7 pentru a fi compatibilă cu mediapipe.

Pentru achizitia de imagini de la camera video pentru antrenarea modelului de recunoastere de obiecte, se va rula createData.py și vor fi salvate într-un folder numit "images". Pentru antreanarea lor se folosește Cascade-Trainer-GUI.
Pentru folosirea modelului, se va reține path-ul către el în variabila cascade_path (ex: cascade_path = "training/classifier/cascade.xml").

Pentru folosirea funcției de LED dimming, este necesară conexiunea prin USB cu o placă Arduino pe portul COM3 (Portul poate fi schimbat după preferință). 

