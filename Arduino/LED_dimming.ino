#include <Wire.h>
#include <LiquidCrystal_I2C.h>


const int ledPin = 9; // Pin number connected to the LED

void setup() {
  pinMode(ledPin, OUTPUT); // Set the LED pin as an output
  Serial.begin(9600); // Set the baud rate
  
  
}

void loop() {
  if (Serial.available() > 0) { // Check if data is available to read
    int brightness = Serial.parseInt(); // Read the incoming data as an integer
    analogWrite(ledPin, brightness); // Set the LED brightness using PWM
    delay(80);
  }
}
